import json
import pytest
import unittest

from src import jqlebra


class JqlebraTest(unittest.TestCase):

    def run_that_thing(self, the_object, the_query, expected_result):
        stringed_object = json.dumps(the_object)
        result = jqlebra.main(stringed_object, the_query)
        self.assertEqual(expected_result, result)

    def test_path(self):
        the_object = {
            "Z1K1": {
                "Z1K1": "Z9",
                "Z9K1": "Z7"
            },
            "Z7K1": {
                "Z1K1": "Z9",
                "Z9K1": "Z801"
            },
            "Z801K1": {
                "Z1K1": "Z6",
                "Z6K1": "o hai"
            }
        }
        self.run_that_thing(the_object, ".Z801K1.Z6K1", "o hai")

    def test_find_errors_normal_form(self):
        the_object = {
            "Z1K1": {
                "Z1K1": {
                    "Z1K1": "Z9",
                    "Z9K1": "Z7"
                },
                "Z7K1": {
                    "Z1K1": "Z9",
                    "Z9K1": "Z883"
                },
                "Z883K1": {
                    "Z1K1": "Z9",
                    "Z9K1": "Z6"
                },
                "Z883K2": {
                    "Z1K1": "Z9",
                    "Z9K1": "Z1"
                }
            },
            "K1": {
                "Z1K1": {
                    "Z1K1": {
                        "Z1K1": "Z9",
                        "Z9K1": "Z7"
                    },
                    "Z7K1": {
                        "Z1K1": "Z9",
                        "Z9K1": "Z881"
                    },
                    "Z881K1": {
                        "Z1K1": {
                            "Z1K1": "Z9",
                            "Z9K1": "Z7"
                        },
                        "Z7K1": {
                            "Z1K1": "Z9",
                            "Z9K1": "Z882"
                        },
                        "Z882K1": {
                            "Z1K1": "Z9",
                            "Z9K1": "Z6"
                        },
                        "Z882K2": {
                            "Z1K1": "Z9",
                            "Z9K1": "Z1"
                        }
                    }
                },
                "K1": {
                    "Z1K1": {
                        "Z1K1": {
                            "Z1K1": "Z9",
                            "Z9K1": "Z7"
                        },
                        "Z7K1": {
                            "Z1K1": "Z9",
                            "Z9K1": "Z882"
                        },
                        "Z883K1": {
                            "Z1K1": "Z9",
                            "Z9K1": "Z6"
                        },
                        "Z883K2": {
                            "Z1K1": "Z9",
                            "Z9K1": "Z1"
                        }
                    },
                    "K1": {
                        "Z1K1": "Z6",
                        "Z6K1": "errors"
                    },
                    "K2": {
                        "Z1K1": {
                            "Z1K1": "Z9",
                            "Z9K1": "Z5"
                        },
                        "Z5K2": {
                            "Z1K1": "Z6",
                            "Z6K1": {}
                        }
                    }
                }
            }
        }
        expected = the_object["K1"]["K1"]["K2"]
        self.run_that_thing(the_object, "finderrors", expected)

    def test_find_errors_canonical_form(self):
        the_object = {
            "Z1K1": {
                "Z1K1": "Z7",
                "Z7K1": "Z883",
                "Z883K1": "Z6",
                "Z883K2": "Z1",
            },
            "K1": [
                {
                    "Z1K1": "Z7",
                    "Z7K1": "Z882",
                    "Z882K1": "Z6",
                    "Z882K2": "Z1",
                },
                {
                    "Z1K1": {
                        "Z1K1": "Z7",
                        "Z7K1": "Z882",
                        "Z882K1": "Z6",
                        "Z882K2": "Z1",
                    },
                    "K1": "errors",
                    "K2": {
                        "Z1K1": "Z5",
                        "Z5K2": "an error"
                    }
                }
            ]
        }
        expected = the_object["K1"][1]["K2"]
        self.run_that_thing(the_object, "finderrors", expected)

    def test_find_no_errors(self):
        the_object = {
            "Z1K1": {
                "Z1K1": "Z7",
                "Z7K1": "Z883",
                "Z883K1": "Z6",
                "Z883K2": "Z1",
            },
            "K1": [
                {
                    "Z1K1": "Z7",
                    "Z7K1": "Z882",
                    "Z882K1": "Z6",
                    "Z882K2": "Z1",
                },
                {
                    "Z1K1": {
                        "Z1K1": "Z7",
                        "Z7K1": "Z882",
                        "Z882K1": "Z6",
                        "Z882K2": "Z1",
                    },
                    "K1": "arrows",
                    "K2": {
                        "Z1K1": "Z5",
                        "Z5K2": "an arrow"
                    }
                }
            ]
        }
        expected = None
        self.run_that_thing(the_object, "finderrors", expected)

    def test_is_void_normal_form(self):
        self.run_that_thing(
            {
                "Z1K1": "Z9",
                "Z9K1": "Z24"
            },
            "isvoid",
            True
        )

    def test_is_void_canonical_form(self):
        self.run_that_thing(
            "Z24",
            "isvoid",
            True
        )

    def test_no_voids_here(self):
        self.run_that_thing(
            "Z23",
            "isvoid",
            False
        )

    def test_from_json(self):
        embedded = {
            "Z1K1": "Z6",
            "Z6K1": "embedded"
        }
        enclosing = {
            "Z1K1": "Z6",
            "Z6K1": json.dumps(embedded)
        }
        self.run_that_thing(
            enclosing,
            " .Z6K1 | fromjson | .Z6K1 ",
            "embedded"
        )

    def test_multiple_queries(self):
        void = {
            "Z1K1": "Z9",
            "Z9K1": "Z24"
        }
        not_void = {
            "Z1K1": "Z9",
            "Z9K1": "Z23"
        }

        def _generate_object(embedded_thing):
            embedded_wrapper = {
                "Z1K1": "Z10000",
                "Z10000K1": json.dumps(embedded_thing)
            }
            enclosing = {
                "Z1K1": "Z6",
                "Z6K1": json.dumps(embedded_wrapper)
            }
            return enclosing

        self.run_that_thing(
            _generate_object(void),
            " .Z6K1 | fromjson | .Z10000K1 | fromjson | isvoid ",
            True
        )
        self.run_that_thing(
            _generate_object(not_void),
            " .Z6K1 | fromjson | .Z10000K1 | fromjson | isvoid ",
            False
        )
