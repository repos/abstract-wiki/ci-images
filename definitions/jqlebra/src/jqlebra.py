"""This is an augmented/jankified version of jq, specialized for ZObjects."""


import json
import re


_JQ_FUNCTIONS = {}


def register_jq(name):
    """Facilitates registration of functions so they can be identified by name.

    When run in the command line, a function registered in this way can be called
    by its {name}.
    """
    def register_decorator(func):
        # Register the function.
        _JQ_FUNCTIONS[name] = func
    return register_decorator


@register_jq("fromjson")
def _from_json(json_string):
    """Similar to jq's fromjson: turns a JSON string into an object."""
    return json.loads(json_string)


@register_jq("isvoid")
def _is_void(json_object):
    """Tests whether a ZObject is a valid Z24 (canonical or normal)."""
    try:
        return json_object.get("Z9K1") == "Z24"
    except AttributeError:
        if json_object == "Z24":
            return True
    return False


def _maybe_convert_zlist_to_list(zlist):
    if isinstance(zlist, list):
        return zlist
    result = []
    tail = zlist
    while tail is not None:
        head = zlist.get("K1")
        if head is not None:
            result.append(head)
        tail = zlist.get("K2")
    return result


def _string_from_z6(maybe_z6):
    try:
        return maybe_z6.get("Z6K1")
    except AttributeError:
        if isinstance(maybe_z6, str):
            return maybe_z6


@register_jq("finderrors")
def _find_errors(Z22K2):
    """Finds the "errors" element of a Z22K2's ZMap."""
    # Z22K2 is a ZMap, so get the ZList at K1.
    tail = Z22K2.get("K1")
    tail = _maybe_convert_zlist_to_list(tail)

    # tail is a ZList and will be a ZList until it's None.
    for element in tail:
        head_head = element.get("K1")
        if head_head is None:
            continue
        if _string_from_z6(head_head) == "errors":
            return element.get("K2")


_QUERY_REGEX = re.compile(r'^(\.\w*)+$')


def main(the_input, query):
    """In jq, paths and functions are separated by pipes; this does the same thing.

    A path looks like
    
        .key1.key2

    A function can be any of the functions whose names are registered with
    register_jq() above.

    Paths and functions can be interleaved by piping the output from one to the
    next.
    """
    if query:
        query_parts = [part.strip() for part in query.split('|')]
    else:
        query_parts = []
    so_far = json.loads(the_input)
    for query_part in query_parts:
        the_function = _JQ_FUNCTIONS.get(query_part)
        if the_function is not None:
            so_far = the_function(so_far)
        elif _QUERY_REGEX.search(query_part):
            keys = query_part.split('.')
            # Get rid of that first empty string.
            _ = keys.pop(0)
            for key in keys:
                if so_far is not None:
                    so_far = so_far.get(key)
        else:
            raise Exception('Bad query: %s', query_part)
    return so_far


if __name__ == '__main__':
    import sys
    the_input = sys.stdin.read()
    if len(sys.argv) > 1:
        the_query = sys.argv[1]
    else:
        the_query = ''
    result = main(the_input, the_query)
    print(json.dumps(result, indent=4))

